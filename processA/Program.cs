﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using OpenCvSharp;

namespace processA {
	class Program {
		[DllImport("kernel32")]
		static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, uint nSize, out uint nNumberOfBytesRead);
		[DllImport("kernel32")]
		static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, uint nSize, out uint nNumberOfBytesWritten);

		static void Main(string[] args) {
			// 다른 프로세스를 읽어온다.
			var procs = Process.GetProcessesByName("processB");
			if(procs.Length != 1) {
				Console.WriteLine("프로세스가 없습니다.");
				return;
			}
			Process procB = procs[0];

			// 메모리 주소, 메모리 크기, 이미지 열 행 입력
			string line = Console.ReadLine();
			string[] values = line.Split(' ');
			// 값 파싱
			// 메모리 주소 값
			int imgMemPtrValue = int.Parse(values[0]);
			// 메모리 크기
			uint imgMemSize = uint.Parse(values[1]);
			// 이미지 행
			int imgRows = int.Parse(values[2]);
			// 이미지 열
			int imgCols = int.Parse(values[3]);
			// 다른 프로세스에서 읽어 올 이미지의 메모리 포인터
			IntPtr imgPtr = new IntPtr(imgMemPtrValue);
			// 읽은 데이터를 출력할 opencv mat 할당
			Mat readImg = new Mat(imgRows, imgCols, MatType.CV_8UC3);
			// 출력할 이미지의 메모리 주소
			IntPtr readImgPtr = readImg.Data;
			// 테스트에서 한번에 다 못 읽을 일은 없을듯 하나...
			uint totalReadLen = 0;
			while(totalReadLen < imgMemSize) {
				ReadProcessMemory(procB.Handle,
					IntPtr.Add(imgPtr, (int)totalReadLen),
					IntPtr.Add(readImgPtr, (int)totalReadLen),
					imgMemSize - totalReadLen, out uint readLen);
				totalReadLen += readLen;
			}
			// 읽은 이미지 확인
			Cv2.ImShow("Read Image", readImg);
			Cv2.WaitKey(0);
			Cv2.DestroyWindow("Read Image");

			// 다른 프로세스의 로그 메모리 주소, 메모리 크기를 읽는다.
			line = Console.ReadLine();
			values = line.Split(' ');
			// 메모리 주소와 크기를 출력해주는데 값을 파싱한다.
			int logMemPtrValue = int.Parse(values[0]);
			uint logMemSize = uint.Parse(values[1]);
			// 다른 프로세스의 로그 메모리 포인터
			IntPtr procBLogPtr = new IntPtr(logMemPtrValue);
			// 출력해줄 로그 값
			string log = "이미지를 성공적으로 읽었습니다.";
			// unicode로 인코딩 된 문자열의 bytes
			byte[] logBytes = Encoding.Unicode.GetBytes(log);
			// 로그 데이터 포인터 할당 및 값 복사
			
			IntPtr logPtr = Marshal.AllocHGlobal(logBytes.Length);
			Marshal.Copy(logBytes, 0, logPtr, logBytes.Length);
			// processB의 로그 메모리에 값을 쓴다.
			uint totalWriteLen = 0;
			while(((logMemSize - totalWriteLen) > 0) && (totalWriteLen < logBytes.Length)) {
				WriteProcessMemory(procB.Handle,
					IntPtr.Add(procBLogPtr, (int)totalWriteLen),
					IntPtr.Add(logPtr, (int)totalWriteLen),
					(logMemSize - totalWriteLen),
					out uint writeLen);
				totalWriteLen += writeLen;
			}

			// 다 썼을을 알리는 신호
			EventWaitHandle ImageDataSignal;
			Trace.Assert(EventWaitHandle.TryOpenExisting("ImageDataSignal", out ImageDataSignal));

			Console.ReadKey();
			// 메모리 해제
			Marshal.FreeHGlobal(logPtr);
		}
	}
}
