﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using OpenCvSharp;

namespace processB {
	class Program {
        static private EventWaitHandle ImageDataSignal
            = new EventWaitHandle(false, EventResetMode.AutoReset, "ImageDataSignal");

        static void Main(string[] args) {
			// 이미지를 불러온다.
			Mat img = Cv2.ImRead("./lol_kda_image.jpg");
			// 이미지 확인용 출력
			Cv2.ImShow("Image", img);
			Cv2.WaitKey(0);
			Cv2.DestroyWindow("Image");

			#region ReadProcessMemory 를 위한 것들
			// 이미지 데이터의 주소
			IntPtr imgPtr = img.Data;
			// 이미지 데이터의 메모리 크기
			uint memSize = ((uint)img.DataEnd - (uint)img.DataStart);

			// 현재 프로세스의 핸들, 이미지 메모리 주소, 
			// 이미지 메모리 크기, 이미지 행 열을 출력
			Console.WriteLine("{0} {1} {2} {3}",
				imgPtr, memSize, img.Rows, img.Cols);
			#endregion ReadProcessMemory 를 위한 것들

			#region WriteProcessMemory 를 위한 것들
			// 메모리 크기를 정한다.
			int bufSize = 128;
			// bufSize 크기로 할당된 메모리 주소
			IntPtr logBufPtr = Marshal.AllocHGlobal(bufSize);
			// 버퍼 메모리 주소, 버퍼 메모리 크기를 출력
			Console.WriteLine("{0} {1}",
				logBufPtr, bufSize);
			#endregion WriteProcessMemory 를 위한 것들

			// 신호를 초기화
			ImageDataSignal.Reset();
			// 다른 프로세스에서 보내는 신호를 기다림.
			ImageDataSignal.WaitOne();
			// 다른 프로세스에서 적은 로그를 받는다.
			Console.WriteLine(Marshal.PtrToStringUni(logBufPtr));
			
			Console.ReadKey();
        }
    }
}
